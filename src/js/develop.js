function dataDropdown(){
    var btn = '[data-dropdown-btn]';
    if(btn.length){
        $(document).on('click', btn, clickHandler);
    }else{
        $(document).off('click', btn, clickHandler);
    }
    function clickHandler(){
        var id = $(this).attr('data-dropdown-btn');
        if(!$(this).hasClass('active')){
            openDD(id);
        }else{
            closeDD(id);
        }
    }
    var close = '[data-dropdown-close]';
    if(close.length){
        $(document).on('click', close, closeHandler);
    }else{
        $(document).off('click', close, closeHandler);
    }
    function closeHandler(){
        var id = $(this).attr('data-dropdown-close');
        if(!$(this).hasClass('active')){
            closeDD(id);
        }
    }
}
function openDD(id) {
    $('[data-dropdown-box]').removeClass('active');
    $('[data-dropdown-btn]').removeClass('active');
    var box = $('[data-dropdown-box='+id+']');
    var btn = $('[data-dropdown-btn='+id+']');
    btn.addClass('active');
    box.addClass('active');
}
function closeDD(id) {
    var box = $('[data-dropdown-box='+id+']');
    var btn = $('[data-dropdown-btn='+id+']');
    btn.removeClass('active');
    box.removeClass('active');
}
function oneScreenScroll() {
    var settings = {
        animationTime: 700,
        infinite: false,
        quietPeriod: 500
    };
    var lastAnimation;
    var slider = $('.js-slider-screen');
    if (slider.length > 0 ){
        window.scrollTop = 0;
        $('.js-slider-screen > *').addClass('tp-screen').eq(0).addClass('tp-screen-active');
        $(document).bind('mousewheel DOMMouseScroll', ishandler);
        var resize = debounce(function() {
            $(document).bind('mousewheel DOMMouseScroll', ishandler);
        }, 250);
        window.addEventListener('resize', resize);
    }
    function ishandler(event) {
        if(window.innerWidth > 992 && $('html').hasClass('desktop')){
            event.preventDefault();
            var delta = event.originalEvent.wheelDelta || -event.originalEvent.detail;
            initScroll(event, delta);
        }else{
            $(document).unbind('mousewheel DOMMouseScroll', ishandler);
        }
    }
    function initScroll(event, delta) {
        var active = document.getElementsByClassName('tp-screen-active')[0];
        var deltaOfInterest = delta,
            timeNow = new Date().getTime();
        if(timeNow - lastAnimation < settings.quietPeriod + settings.animationTime) {
            return;
        }
        if(window.innerWidth > document.body.clientWidth){
            if(window.scrollY < active.offsetHeight - window.innerHeight && deltaOfInterest < 0) return;
            if(window.scrollY > 0 && deltaOfInterest > 0) return;
        }
        if (deltaOfInterest < 0) {
            moveNext();
        } else {
            movePrev();
        }
        lastAnimation = timeNow;
    }

    $(document).on('click','.js-slider-screen-downbutton', moveNext);
    function moveNext() {
        var el = $('.tp-screen-active');
        var next = el.next();
        if(next.length > 0){
            el.removeClass('tp-screen-active');
            next.addClass('tp-screen-active');
            setTimeout(  afterShow , 100);
        }else if(settings.infinite) {
            next = $('.tp-screen:first-child');
            el.removeClass('tp-screen-active');
            next.addClass('tp-screen-active');
            setTimeout(  afterShow ,100);
        }
    }
    function movePrev() {
        var el = $('.tp-screen-active');
        var prev =   el.prev();
        if(prev.length > 0){
            el.removeClass('tp-screen-active');
            prev.addClass('tp-screen-active');
            setTimeout(  afterShow , 100);
        }else if(settings.infinite) {
            prev = $('.tp-screen:last-child');
            el.removeClass('tp-screen-active');
            prev.addClass('tp-screen-active');
            setTimeout(  afterShow , 100);
        }
    }
    function afterShow() { //callback function

    }

}